#!/bin/sh

mvn install:install-file \
	-Dfile=../lib/pdfbox-app-1.8.5.jar \
	-DgroupId=apache.pdfbox \
	-DartifactId=pdfbox \
	-Dversion=1.8.5 \
	-Dpackaging=jar

mvn install:install-file \
	-Dfile=../lib/bibtexconverter.jar \
	-DgroupId=net.sourceforge \
	-DartifactId=bibtexml \
	-Dversion=0.5.3 \
	-Dpackaging=jar

mvn install:install-file \
	-Dfile=../lib/icepdf-core.jar \
	-DgroupId=org.icepdf.pdf \
	-DartifactId=icepdf-core \
	-Dversion=5.0.2 \
	-Dpackaging=jar

mvn install:install-file \
	-Dfile=../lib/icepdf-viewer.jar \
	-DgroupId=org.icepdf.pdf \
	-DartifactId=icepdf-viewer \
	-Dversion=5.0.2 \
	-Dpackaging=jar

