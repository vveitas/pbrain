package lt.vveitas.pbrain;

import org.junit.After;
import org.junit.Test
import org.junit.Ignore
import org.junit.Before
import org.slf4j.Logger;

import static org.junit.Assert.*

import lt.vveitas.pbrain.Utils

class ParserTest {
	
	@Test
	public void testParser() {
		String libraryfilexml = 'library/library.xml'
		def entries = Utils.parseXML(libraryfilexml)
		println entries
	}
	
	@Test
	public void testBibTeXConverter() {
		Utils.convertBibToXML()
	}
	
}
