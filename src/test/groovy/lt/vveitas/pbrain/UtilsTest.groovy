package lt.vveitas.pbrain;

import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.junit.Test
import org.junit.Ignore

import static org.junit.Assert.*

import lt.vveitas.pbrain.Utils

class UtilsTest {

	@Ignore
	@Test
	public void testSerializePDRectangle() {
		PDRectangle r1 = new PDRectangle()
		r1.setLowerLeftX(1f)
		r1.setLowerLeftY(2f)
		r1.setUpperRightX(3f)
		r1.setUpperRightY(4f)
		
		def string = Utils.serialize(r1)
		assert string == "[1.0,2.0,3.0,4.0]"
		
		def r2 = Utils.deserializePDRectangle(string)
		assert r2.class == PDRectangle
		assert Utils.serialize(r2) == Utils.serialize(r1)
	}
	
	@Test
	public void testCalculateMD5Hex() {
		String hash = Utils.calculateMD5Hex("Norvig.1992.ParadigmsOfAIProgramming.pdf")
		println hash
	}
	
}
