package lt.vveitas.pbrain;

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test
import org.junit.Before
import org.slf4j.Logger;

import java.awt.image.BufferedImage;

import static org.junit.Assert.*

import lt.vveitas.pbrain.Utils

class PDFMinerTest {
	
	@Test
	public void testGetDocumentAnnotations() {
		String file = "/home/vveitas/books/full.pdf";
		
		try {
			
			def annotations = PDFMiner.getDocumentAnnotations(file);
			
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void cropRectangleToImage() {
		BufferedImage imagebi
		String file = "/home/vveitas/tmp/13.pdf";
		PDDocument document = PDDocument.load(new File(file));
		PDPage page =  document.getDocumentCatalog().getAllPages().get(0);
		List<PDAnnotation> la = page.getAnnotations();
		def rectangle
		for (int x=0; x<la.size();x++) {
			def ann = la.get(x);
			def subtype = ann.getSubtype()
			if (subtype.equals("Square")) {
				rectangle = ann.getRectangle()
				imagebi = PDFMiner.imageFromByteArray(PDFMiner.cropRectangleToImage(page, rectangle))
			}
		}
		def window = new ImageWindow(imagebi)
		   System.in.withReader {
			   println "Enter any key to finish"
			   it.readLine()
    }
	}
	
}
