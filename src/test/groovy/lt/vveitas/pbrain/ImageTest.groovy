package lt.vveitas.pbrain;

import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.pdmodel.PDPage
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation
import org.junit.After;
import org.junit.Test
import org.junit.Ignore
import org.junit.Before
import org.slf4j.Logger;

import static org.junit.Assert.*

import com.thinkaurelius.titan.core.TitanVertex
import com.tinkerpop.blueprints.Vertex
import java.awt.image.BufferedImage
import lt.vveitas.pbrain.Utils

class ImageTest {
	
	@Ignore
	@Test
	public void testPutImageToGraph() {
		byte[] imageba
		String file = "/home/vveitas/tmp/13.pdf";
		PDDocument document = PDDocument.load(new File(file));
		PDPage page =  document.getDocumentCatalog().getAllPages().get(0);
		List<PDAnnotation> la = page.getAnnotations();
		def rectangle
		for (int x=0; x<la.size();x++) {
			def ann = la.get(x);
			def subtype = ann.getSubtype()
			if (subtype.equals("Square")) {
				rectangle = ann.getRectangle()
				imageba = PDFMiner.cropRectangleToImage(page, rectangle)
			}
		}
		def network = new Network()
		Vertex vertex = network.g.addVertex()
		vertex.setProperty("type", "image")
		vertex.setProperty("image", imageba) // this line does not pass - cannot write
		
		assert vertex.getProperty("image") == imageba 
	}
	
	@Test
	public void testSaveImageToDisk() {
		byte[] imageba
		String file = "/home/vveitas/tmp/13.pdf";
		PDDocument document = PDDocument.load(new File(file));
		PDPage page =  document.getDocumentCatalog().getAllPages().get(0);
		List<PDAnnotation> la = page.getAnnotations();
		def rectangle
		for (int x=0; x<la.size();x++) {
			def ann = la.get(x);
			def subtype = ann.getSubtype()
			if (subtype.equals("Square")) {
				rectangle = ann.getRectangle()
				println (PDFMiner.saveImageToFile(page, rectangle))
			}
		}
	}

	
}
