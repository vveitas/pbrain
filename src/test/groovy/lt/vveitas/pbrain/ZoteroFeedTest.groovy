package lt.vveitas.pbrain;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed
import org.junit.After;
import org.junit.Test
import org.junit.Ignore
import org.junit.Before
import org.slf4j.Logger;

import static org.junit.Assert.*
import lt.vveitas.pbrain.Utils

import org.jdom.Document as JDOMDocument

class ZoteroFeedTest {
	
	@Ignore
	@Test
	public void testFullLibraryStream() {
		List<Entry> library = Utils.getZoteroLibrary()
	}

	@Ignore
	@Test
	public void testGetKeys() {
		List<Entry> library = Utils.getZoteroLibrary()
		List<String> keys = Utils.getZoteroItemKeys(library)
		println keys
	}

	@Test
	public void testGetBibtexKeys() {
		List<Entry> library = Utils.getZoteroLibrary()
		List<String> keys = Utils.getZoteroItemKeys(library)
		Map<String,String> bibkeys = Utils.getBibtexKeys(keys)
		println bibkeys

	}
	
	@Test
	public void testGetFileNames() {
		List<Entry> library = Utils.getZoteroLibrary()
		List<String> keys = Utils.getZoteroItemKeys(library)
		Map<String,String> filenames = Utils.getFileNames(keys)
		println filenames
	}
	
	
	@Test
	public void testGetBibtexEntries() {
		List<Entry> library = Utils.getZoteroLibrary()
		List<String> keys = Utils.getZoteroItemKeys(library)
		Map<String,JDOMDocument> bibtexEntries = Utils.getBibtexEntries(keys)
		println bibtexEntries
	}
	
	@Ignore
	@Test
	public void testParseEntries() {
		List<Entry> library = Utils.getZoteroLibrary()
		Entry entry = library[0]
		def parsed = new XmlSlurper().parseText(entry.toString())
		Map<String,String> attributes = [:]
		parsed.content.div.table.tr.each{
			attributes.put(it.th.text(), it.td.text())
		}
		println(attributes)
		println(parsed.title[0])
		println(parsed.author[0].name[0])
	}
	
}
