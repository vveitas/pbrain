package lt.vveitas.pbrain.brainbrowser

import lt.vveitas.pbrain.DataPanel;

import org.junit.Test

class DataPanelTest {
	
	@Test
	public void testDataPanel() {
		def control = new DataPanel([publications:['kegan_evolving_1982']])
		System.in.withReader {
			println "Enter any key to finish"
			it.readLine()
		}
	}
}