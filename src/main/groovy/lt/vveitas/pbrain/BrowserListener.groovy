package lt.vveitas.pbrain;

import java.awt.event.MouseEvent;

import org.graphstream.ui.swingViewer.ViewerListener;

public class BrowserListener implements ViewerListener{
	GraphStreamSource source
	String nodeId
	
	public BrowserListener(GraphStreamSource source) {
		this.source = source
	}

	public void viewClosed(String id) {
		// not implemented
	}
	
	public void buttonPushed(String id) {
		this.nodeId = id
	}
 
	public void buttonReleased(String id) {
	}
	
	public void reset() {
		this.nodeId == null
	}
	
}