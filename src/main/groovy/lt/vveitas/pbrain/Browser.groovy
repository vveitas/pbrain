package lt.vveitas.pbrain;

import com.tinkerpop.blueprints.Vertex

import groovy.swing.SwingBuilder

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

import java.awt.Dimension
import java.awt.GridLayout
import java.awt.Color
import java.awt.Image
import java.awt.Panel;
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.graphstream.graph.Graph
import org.graphstream.graph.implementations.MultiGraph
import org.graphstream.stream.thread.ThreadProxyPipe
import org.graphstream.ui.spriteManager.SpriteManager
import org.graphstream.ui.swingViewer.View
import org.graphstream.ui.swingViewer.Viewer
import org.graphstream.ui.swingViewer.ViewerListener
import org.graphstream.ui.swingViewer.ViewerPipe
import org.graphstream.ui.spriteManager.* 

import javax.swing.text.View as SwingView
import org.imgscalr.Scalr

import java.awt.MouseInfo
import java.awt.Point

public class Browser {
	public GraphStreamSource source
	private Graph graph
	SwingBuilder swing
	JFrame browserWindow
	
	public Browser(Vertex vertex) {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		String css = new File("styles/graph_stylesheet.css").text
		
		def xposition
		def yposition
		
		this.source = new GraphStreamSource()
		this.source.registerEdgeProperties(["label"])
		this.source.registerVertexProperties(["text", "contents","author","key"])
		
		graph = new MultiGraph("multigraph")
		graph.addAttribute("ui.stylesheet", css)
		graph.addAttribute("ui.quality")
		graph.addAttribute("ui.antialias")
		SpriteManager sman = new SpriteManager(graph)
		
		Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD)
		viewer.enableAutoLayout()

		this.source.addSink(graph)
		this.source.createViewFromVertex(vertex)
		this.source.pump()

		View view = viewer.addDefaultView(false)
		view.resizeFrame(600, 600);
		
		def browserListener = new BrowserListener(source)
		
		ViewerPipe fromViewer = viewer.newViewerPipe()
		fromViewer.addViewerListener(browserListener)
		fromViewer.addSink(graph)
		
		this.swing = new SwingBuilder()
		
		browserWindow = swing.frame(title: 'Browser', 
							defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE, 
							size:[600,600],
							show: true) {
		}
		browserWindow.add(view)
		
		def displayText = { html ->
			def frame = swing.frame(title: 'Annotation', 
				defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE,
				undecorated: true)
			 
			def label = new JLabel()
			
			label.setText(html);
				
			def prefSize = 400
			def width = true
				
			SwingView textview = (SwingView) label.getClientProperty(javax.swing.plaf.basic.BasicHTML.propertyKey);
			textview.setSize(width?prefSize:0,width?0:prefSize);
							
			float w = textview.getPreferredSpan(SwingView.X_AXIS);
			float h = textview.getPreferredSpan(SwingView.Y_AXIS);
			def dimenion = new java.awt.Dimension((int) Math.ceil(w), (int) Math.ceil(h));
			
			frame.getContentPane().add(label)
			frame.pack()
			
			Point mouseLocation = MouseInfo.getPointerInfo().getLocation()
			frame.setLocation(mouseLocation)
			frame.setAlwaysOnTop(true)
			frame.setVisible(true)
			
			MouseListener closeOnClick = new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					def clickCount = e.getClickCount()
					def button = e.getButton()
					if (clickCount == 1 && button == 1) {
						frame.dispose()
					}
				}
			};
			frame.addMouseListener(closeOnClick)
		}

		def displayImage = { BufferedImage bufferedImage ->
			def frame = swing.frame(title: 'Image',
				defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE,
				undecorated: true) {
				
				def width = bufferedImage.getWidth()
				def height = bufferedImage.getHeight()
				
				bufferedImage = Scalr.resize(bufferedImage, Scalr.Mode.AUTOMATIC,(int) width/2, (int) height/2);
				
				label(icon:imageIcon(bufferedImage))
			
			}
			
			Point mouseLocation = MouseInfo.getPointerInfo().getLocation()
			frame.setLocation(mouseLocation)
			
			frame.pack()
			frame.setAlwaysOnTop(true)
			frame.setVisible(true)
			
			MouseListener closeOnClick = new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					def clickCount = e.getClickCount()
					def button = e.getButton()
					if (clickCount == 1 && button == 1) {
						frame.dispose()
					}
				}
			};
			frame.addMouseListener(closeOnClick)
		}

				
		def displayFullAnnotation = { nodeId ->
			def annotationContents = source.getAnnotationContents(nodeId)
			def contentsClass = annotationContents.getClass().getName()
			if (contentsClass == "java.lang.String") {
				displayText("<html><p>" + annotationContents + "</p></html>")
			} else if (contentsClass == "java.awt.image.BufferedImage") {
				displayImage(annotationContents)
			}
		}
		
		def displayVertexId = { nodeId -> 
			def properties = source.getVertexProperties(nodeId)
			displayText(properties)
		}
		
		def menuItems = {
			menuItem() {
				action( name:'Open Document', mnemonic:'O', closure: { source.openDocument(browserListener.nodeId) }) 
			}
			menuItem() {
				action( name:'Expand', mnemonic:'E', closure: { source.expandFromVertex(browserListener.nodeId) } ) 
			}
			menuItem() {
				action( name:'Display Full Annotation', mnemonic:'E', closure: { displayFullAnnotation(browserListener.nodeId) } ) 
			}
			menuItem() {
				action( name:'Vertex Id', mnemonic:'V', closure: { displayVertexId(browserListener.nodeId) } )
			}
		}
		
		def popup = {
			swing.popupMenu(menuItems)
		}
		
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				def action
				def clickCount = e.getClickCount()
				def button = e.getButton()
				fromViewer.pump()
				if (clickCount == 1 && button == 1) {
					xposition = e.getX()
					yposition = e.getY()
					swing.popupMenu(menuItems).show(e.getComponent(), e.getX(), e.getY())
				}
			}
		};

		view.addMouseListener(mouseListener)
		
	}
	
}
