package lt.vveitas.pbrain;

import java.awt.*;
import java.awt.image.BufferedImage;

public class AnnotationImage extends Panel {
  public BufferedImage image;
  public int height;
  public int width;
  
  public AnnotationImage(BufferedImage image) {
		  this.image = image;
		  this.width = image.getWidth();
		  this.height = image.getHeight();
  }
  
  public void paint() {
	  image.getGraphics().drawImage( image, 0, 0, null);
  }
  
}