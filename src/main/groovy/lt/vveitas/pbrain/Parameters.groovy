package lt.vveitas.pbrain

class Parameters {
	/*
	 * Following two lines are hardcoded absolute path and filename of the configuration file
	 * It is possible to make searching relative to source folder, yet I am still not sure which way is better....
	 * There is one possible threat for this, explained somewhere here:
	 * http://stackoverflow.com/questions/4646577/global-variables-in-java
	 * (garbage collector may unload the class and then all the variables will be lost...) 
	 * 
	 */
	
	// read global parameters from the config file
	static configurationsPath = "configs/"
	static configFileName = "pbrain.conf"
	
	public static Map parameters = new ConfigSlurper('config').parse(new File(configurationsPath + configFileName).toURI().toURL())
	public static Map zotero = new ConfigSlurper('zotero').parse(new File(this.parameters.main_dir \
		+ this.parameters.pbrain_dir \
		+ this.parameters.zotero_conf).toURI().toURL())
	
}
