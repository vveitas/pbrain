package lt.vveitas.pbrain;

import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.util.PDFTextStripperByArea;

public class Annotation {
	HashMap<Object,Object> parameters;
	
	Annotation(){
		this.parameters=new HashMap<Object,Object>();
	}
	
	public String extractHighlightedText(PDPage page) {
        try {
            List quadPoints = this.parameters.get("quadPoints")
            def lines = quadPoints.size().intdiv(8)
            def highlightedText = ""
            for (int x = 0; x < lines; x++) {
                float ulx = quadPoints[x * 8] - 1
                float uly = quadPoints[x * 8 + 1]
                float urx = quadPoints[x * 8 + 2] + 1
                float lly = quadPoints[x * 8 + 5] + 1
                float height = uly - lly
                float width = urx - ulx
//			assert height>= 0;
//			assert width>=0;

                PDRectangle pageSize = page.findMediaBox();
                uly = pageSize.getHeight() - uly - 1;

                Rectangle2D.Float awtRect = new Rectangle2D.Float(ulx, uly, width, height);

                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                stripper.addRegion(Integer.toString(0), awtRect);
                stripper.extractRegions(page);
                highlightedText = highlightedText + stripper.getTextForRegion(Integer.toString(0)) + " ";
                highlightedText = highlightedText.replace("\n", "")
                highlightedText = highlightedText.replace("- ", "")
            }
            return highlightedText;
        }catch(Throwable e){
            e.printStackTrace()
        }
	}
	
	public String extractHighlightedTextAsRectangle(PDPage page) {
        try {
            def highlightedText = ""
            try {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);

                PDRectangle rect = this.parameters.get("Rectangle");
                float x = rect.getLowerLeftX() - 1;
                float y = rect.getUpperRightY() - 1;
                float width = rect.getWidth() + 2;
                float height = rect.getHeight() + rect.getHeight() / 4;
                int rotation = page.findRotation();
                if (rotation == 0) {
                    PDRectangle pageSize = page.findMediaBox();
                    y = pageSize.getHeight() - y;
                }

                Rectangle2D.Float awtRect = new Rectangle2D.Float(x, y, width, height);
                stripper.addRegion(Integer.toString(0), awtRect);
                stripper.extractRegions(page);
                //
                System.out.println(stripper.getTextForRegion(Integer.toString(0)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            highlightedText = highlightedText.replace("\n", "")
            highlightedText = highlightedText.replace("- ", "")
            return highlightedText
        }catch(Throwable e){
            e.printStackTrace()
        }
	}
}