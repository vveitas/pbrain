package lt.vveitas.pbrain

import lt.vveitas.pbrain.Network

public class Globals {
	
	public static Network network = new Network()
	public static openFrames = []
	
	public void ass() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
			  network.g.commit()
			  network.g.shutdown()
			}
		  });
	}

}
