package lt.vveitas.pbrain;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import net.sourceforge.bibtexml.BibTeXConverter;

import org.apache.abdera.model.Document;
import org.apache.abdera.model.Feed;
import org.apache.abdera.model.Entry;
import org.apache.pdfbox.pdmodel.common.PDRectangle

import java.awt.image.BufferedImage;

import groovy.swing.SwingBuilder

import org.apache.abdera.protocol.client.AbderaClient
import org.apache.abdera.protocol.client.ClientResponse
import org.apache.abdera.protocol.client.RequestOptions
import org.apache.abdera.protocol.Response.ResponseType
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import java.awt.BorderLayout as BL
import java.awt.Desktop

import org.jdom.Document as JDOMDocument
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.json.JsonSlurper

public class Utils {
	private static Logger logger = LoggerFactory.getLogger(Utils.class)
	
	public static Object parseXML(String libfilexml) {
		def library = new XmlSlurper().parse(new File(libfilexml))
		return library
	}
	
	public static void convertBibToXML() {
		def bibFileName = Parameters.parameters.main_dir + Parameters.parameters.library_bib
		def xmlFileName = Parameters.parameters.main_dir + Parameters.parameters.library_xml
		File FileIn = new File(bibFileName)
		File FileOut = new File(xmlFileName)
		def converter = new BibTeXConverter()
		converter.bibTexToXml(FileIn, FileOut)
	}
	
	public static String serialize(PDRectangle rectangle) {
		return rectangle.toString()
	}
	
	public static PDRectangle deserializePDRectangle(String string) {
		ArrayList list = Eval.me(string)
		def rect = new PDRectangle()
		rect.setLowerLeftX(list[0])
		rect.setLowerLeftY(list[1])
		rect.setUpperRightX(list[2])
		rect.setUpperRightY(list[3])
		return rect
	}
	
	public static PDRectangle expandRectangleBy(PDRectangle rectangle, float expansion) {
		PDRectangle expandedRectangle = new PDRectangle()
		expandedRectangle.setLowerLeftX((float) rectangle.getLowerLeftX()-expansion)
		expandedRectangle.setLowerLeftY((float) rectangle.getLowerLeftY()-expansion)
		expandedRectangle.setUpperRightX((float) rectangle.getUpperRightX()+expansion)
		expandedRectangle.setUpperRightY((float) rectangle.getUpperRightY()+expansion)
		return expandedRectangle
	}
	
	public static void createDirIfNeeded(File dir) {
		
		// If it doesn't exist
		if( !dir.exists() ) {
		  // Create all folders up-to and including B
		  dir.mkdirs()
		}
		
	}
	
	public static List<String> getAllTags() {
		String zotero_user = Parameters.zotero.zotero_user
		String zotero_collection = Parameters.zotero.zotero_collection
		String zotero_key = Parameters.zotero.zotero_key
		
		String query = "https://api.zotero.org/users/" +zotero_user + "/collections/" + zotero_collection + "/tags?key=" + zotero_key

        String curl_query = """ curl -H Zotero-API-version: 2 ${query} """

        def proc = curl_query.execute()
        proc.waitFor()

        def xml = new XmlSlurper().parseText(proc.in.text)

        List<String> tagList = new ArrayList<String>()
        xml.entry.each { entry ->
            if (entry.title.text().contains("vveitas:")) {tagList.add(entry.title)}
        }

        return tagList
    }

	public static ArrayList getZoteroLibrary(String project) {
		String zotero_user = Parameters.zotero.zotero_user
		String zotero_collection = Parameters.zotero.zotero_collection
		String zotero_key = Parameters.zotero.zotero_key
		
		AbderaClient client = new AbderaClient()
		AbderaClient.registerTrustManager();
		
		def metadata
		String metadata_query = 'https://api.zotero.org/users/'\
			+zotero_user+'/collections/'+zotero_collection+'/items/top?format=atom&limit=1&key='+zotero_key

		logger.debug("Issuing query: {}", metadata_query)
		ClientResponse response = client.get(metadata_query)
		if (response.getType() == ResponseType.SUCCESS) {
			metadata = response.getInputStream();
		} else {
			System.out.println("There was an error reading Atom stream from Zotero")
		}
		logger.debug("Got response: {}", response.toString())
		
		def meta = new XmlSlurper().parse(metadata)
		int totalResults = meta.totalResults.text().toInteger() 
		
		def library_file = new FileWriter(Parameters.parameters.library_xml)



		def library = []
		List starts = (0..totalResults).step(100)
		starts[0] = starts[0]
		starts.each{start ->
			def chunk
            String chunk_query
            if (project == null) {
                chunk_query = 'https://api.zotero.org/users/' + zotero_user+ '/collections/'+ zotero_collection\
				+ '/items/top?format=json&limit=100&start='+start+ '&key='+ zotero_key
            } else {
                chunk_query = 'https://api.zotero.org/users/' + zotero_user+ '/collections/'+ zotero_collection\
				+ '/items/top?tag='+project+ '&format=json&limit=100&start='+start+ '&key='+ zotero_key
            }

            URL url = new URL(chunk_query);
            InputStream urlStream = null;
            urlStream = url.openStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlStream));
            chunk = new JsonSlurper().parse(reader)

			library = library + chunk
		}

		return library
	}
	
	public static List<String> getZoteroItemKeys(ArrayList entries) {
		List<String> keys = new ArrayList<String>()
		entries.data.key.each{ key -> keys.add(key) }
		return keys
	}
	
	public static String getZoteroItemKey(Map entry) {
		return entry.key
	}

	
	public static Map<String, String> getBibtexKeys(List<String> keys) {
		Map<String,String> bibtex = new HashMap<String,String>()
		String zotero_user = Parameters.zotero.zotero_user
		String zotero_key = Parameters.zotero.zotero_key
		
		AbderaClient client = new AbderaClient()
		AbderaClient.registerTrustManager();
		keys.each {itemKey ->
				String bibentry_query = 'https://api.zotero.org/users/'\
						+zotero_user+'/items/'+itemKey+'/?format=bibtex&key='+zotero_key
				RequestOptions ro = new RequestOptions()
				ro.setHeader("Zotero-API-Version", "2")
				ClientResponse response = client.get(bibentry_query, ro)
				if (response.getType() == ResponseType.SUCCESS) {
						InputStream is = response.getInputStream()
						String bibtexEntry = getStringFromInputStream(is)
						def start = bibtexEntry.indexOf("{") + 1
						def finish = bibtexEntry.indexOf(",")
						bibtex.put(itemKey,bibtexEntry.substring(start,finish))
				} else {
						System.out.println("There was an error reading Atom stream from Zotero in getBibtexKeys")
				}
		}
		return bibtex
	}

    /*
    *This is going to be extremely inneficient, but just for now.
    * In the future I probably have to go for JSON and get rid of Atom and Abdera stuff, but that
    * will require rewriting all classes related to getting data from Zotero.
    */
    public static Map<String, ArrayList<String>> getPublicationTags(String publication_key) {
        Map<String, ArrayList<String>> projects = new HashMap<String, ArrayList<String>>()
        String zotero_user = Parameters.zotero.zotero_user
        String zotero_key = Parameters.zotero.zotero_key
        String zotero_collection = Parameters.zotero.zotero_collection


        String query = "https://api.zotero.org/users/${zotero_user}/items/${publication_key}/tags?format=json&key=${zotero_key}"

        URL url = new URL(query);
        InputStream urlStream = null;
        urlStream = url.openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(urlStream));
        def xml
        xml = new JsonSlurper().parse(reader)

        List<String> tagList = new ArrayList<String>()
        xml[0].each { entry ->
            List<String> projectList = new ArrayList<String>()
            if (entry.key == 'tag' && entry.value.contains("vveitas:")) {
                projectList.add(entry.value)
            }
            projects.put(publication_key, projectList)
        }

        return projects
    }

	public static Map<String, JDOMDocument> getBibtexEntries(List<String> keys) {
		Map<String,String> bibtex = new HashMap<String,String>()
		String zotero_user = Parameters.zotero.zotero_user
		String zotero_key = Parameters.zotero.zotero_key
		def converter = new BibTeXConverter()
		
		AbderaClient client = new AbderaClient()
		AbderaClient.registerTrustManager();
		keys.each {itemKey ->
				String bibentry_query = 'https://api.zotero.org/users/'\
						+zotero_user+'/items/'+itemKey+'/?format=bibtex&key='+zotero_key
				logger.debug("Issuing query: {}, bibentry_query")
						
				RequestOptions ro = new RequestOptions()
				ro.setHeader("Zotero-API-Version", "2")
				ClientResponse response = client.get(bibentry_query, ro)
				logger.debug("Got response: {}", response.toString())
				if (response.getType() == ResponseType.SUCCESS) {
						InputStream is = response.getInputStream()
						File file = Utils.stream2file(is)
						JDOMDocument bibtexml = converter.bibTexToXml(file)
						bibtex.put(itemKey,bibtexml)
				} else {
						System.out.println("There was an error reading Atom stream from Zotero in GetBibtexEntries")
				}
		}
		return bibtex
	}
	
	public static Map<String, String> getFileNames(List<String> keys) {
		Map<String,String> files = new HashMap<String,String>()
		String zotero_user = Parameters.zotero.zotero_user
		String zotero_key = Parameters.zotero.zotero_key
		
		AbderaClient client = new AbderaClient()
		AbderaClient.registerTrustManager();
		keys.each {itemKey ->
			String children_query = 'https://api.zotero.org/users/'\
			+zotero_user+'/items/'+itemKey+'/children?key='+zotero_key
			logger.debug("Issuing query: {}, children_query")
			Document<Feed> children
			logger.debug("Got response: {}, children.toSting()")
			RequestOptions ro = new RequestOptions()
			ro.setHeader("Zotero-API-Version", "2")
			ClientResponse response = client.get(children_query, ro)
			if (response.getType() == ResponseType.SUCCESS) {
				children = response.getDocument();
			} else {
				System.out.println("There was an error reading Atom stream from Zotero in getFileNames")
			}
			children.getRoot().getEntries().each{Entry entry ->
				String title = entry.getTitle()
				if (title.lastIndexOf(".") != -1
					&& title.substring(title.lastIndexOf(".")+1) == "pdf") {
					files.put(itemKey, title)
				}
			}
		}
		return files
	}

	
		
	// http://www.mkyong.com/java/how-to-convert-inputstream-to-string-in-java/
	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 
	}
	
	/*
	 * http://stackoverflow.com/questions/4317035/how-to-convert-inputstream-to-virtual-file
	 */
	public static File stream2file(InputStream instream) throws IOException {
		final String PREFIX = "stream2file";
		final String SUFFIX = ".tmp";
		
		final File tempFile = File.createTempFile(PREFIX, SUFFIX)
		tempFile.deleteOnExit()
		FileOutputStream out = new FileOutputStream(tempFile)
		IOUtils.copy(instream, out);
		return tempFile;
	}
	
	private static String calculateMD5Hex(String filename) {
		String library_dir = Parameters.parameters.main_dir + Parameters.parameters.library_dir
		InputStream fileis = new FileInputStream(library_dir + filename) 
		String hash = DigestUtils.md5Hex(fileis)
		return hash
	}
	
	public Object getEntry(String) {
		
	}
	
	
	public static void openPDFFile(String fileName) {
		if (Desktop.isDesktopSupported()) {
			try {
				String library_dir = Parameters.parameters.library_dir +"/" 
				File myFile = new File(library_dir + fileName);
				Desktop.getDesktop().open(myFile);
			} catch (IOException ex) {
				// no application registered for PDFs
			}
		}
	}
	
	public static openDocumentEvince(String fileName, int page) {
		String main_dir = Parameters.parameters.main_dir
		String library_dir = main_dir + Parameters.parameters.library_dir
		String command = 'evince '	+ '--page-label=' + page + ' ' + library_dir + fileName
		command.execute()
	}
	
	public static openDocumentOkular(String fileName) {
		String main_dir = Parameters.parameters.main_dir
		String library_dir = main_dir + Parameters.parameters.library_dir
		String command = 'okular '	+ library_dir + fileName
		command.execute()
	}

	public static openDocumentAcroread(String fileName) {
		String main_dir = Parameters.parameters.main_dir
		String library_dir = main_dir + Parameters.parameters.library_dir
		String command = 'acroread ' + library_dir + fileName
		command.execute()
	}

	
}
