package lt.vveitas.pbrain

import java.io.File;

import org.apache.commons.configuration.Configuration
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.PropertiesConfiguration
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.graphstream.stream.thread.ThreadProxyPipe

import com.fasterxml.jackson.databind.util.Annotations;
import com.thinkaurelius.titan.core.TitanGraph
import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanVertex
import com.tinkerpop.blueprints.Graph
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.util.wrappers.event.EventGraph
import com.thinkaurelius.titan.core.attribute.Text

import static org.junit.Assert.*

import com.tinkerpop.gremlin.groovy.Gremlin
import com.tinkerpop.pipes.Pipe

import org.apache.abdera.model.Entry;
import org.apache.abdera.parser.stax.FOMEntry

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Network {
	static {
		Gremlin.load()
	  }
	
	private static Logger logger = LoggerFactory.getLogger(Network.class)
	
	public TitanGraph g
	private Map indices = [:]
	
	public Network() {

		String graphDatabase = Parameters.parameters.graphDatabase
		String graph_dir =  Parameters.parameters.main_dir + Parameters.parameters.pbrain_dir + Parameters.parameters.graph_dir
		String searchindex_dir =  Parameters.parameters.main_dir + Parameters.parameters.pbrain_dir + Parameters.parameters.searchindex_dir
				
		switch ( graphDatabase ) {
			case "TitanEmbedded":
				//Configuration conf = new PropertiesConfiguration("titan_embedded.conf")
				Configuration conf = new BaseConfiguration();
				conf.setProperty("storage.backend","berkeleyje");
				conf.setProperty("storage.directory",graph_dir);
				conf.setProperty("storage.index.search.backend","elasticsearch");
				conf.setProperty("storage.index.search.directory",searchindex_dir);
				conf.setProperty("storage.index.search.client-only",false);
				conf.setProperty("storage.index.search.local-mode",true);
				
				this.g = TitanFactory.open(conf)
				break
		}
		assertNotNull(g)
		if (g.getType("type") == null) init()
		
	}
	
	protected void init() {
		
		// index edges by their creation dates
		// a good idea would be also indexing edges by their importance, but this is later probably.
		def creationDate = this.g.makeKey('creationDate').dataType(Date.class).indexed(Vertex.class).unique().make()
		//def creationDate = this.g.makeType().name('creationDate').unique(Direction.OUT).dataType(Date.class).makePropertyKey()
		this.g.makeLabel("isAssociated").sortKey(creationDate).make()
		//this.g.makeType().name("isAssociated").primaryKey(creationDate).makeEdgeLabel()
		
		// create types for searching
		this.g.makeKey("type").dataType(String.class).indexed(Vertex.class).make()
		this.g.makeKey("key").dataType(String.class).indexed(Vertex.class).make()
		this.g.makeKey("entry").dataType(String.class).indexed(Vertex.class).make()
		this.g.makeKey("year").dataType(String.class).indexed(Vertex.class).unique().make()
		
		this.g.makeKey("authorstext").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make()
		this.g.makeKey("editorstext").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make()
		this.g.makeKey("contents").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make();
		this.g.makeKey("text").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make();
		this.g.makeKey("title").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make();
		this.g.makeKey("abstract").dataType(String.class).indexed(Vertex.class).indexed("search", Vertex.class).make();

		this.g.commit()
		
	}
	
	public Vertex updatePublicationVertex(Map publication,
											String zotero_key, 
											String bib_key, 
											String filename) {

        def projects = Utils.getPublicationTags(zotero_key)
		//def publication = new XmlSlurper().parseText(entry.toString())
		def zotero_database_timestamp = publication.data.dateModified
		
		Vertex vertex = g.query().has("key",zotero_key).vertices().isEmpty()\
		? g.addVertex()\
		: g.query().has("key",zotero_key).vertices().iterator().next()

		if (zotero_database_timestamp != vertex.getProperty('zotero_timestamp')) {
			vertex.setProperty("entry", "publication")
			vertex.setProperty("bib_key", bib_key)
			vertex.setProperty("file", filename)
			vertex.setProperty("title", publication.data.title)
			vertex.setProperty("zotero_timestamp", zotero_database_timestamp)
			vertex.setProperty("type", publication.data.itemType)
            def authors = []
            def editors = []
            publication.data.each{property->
                switch (property.key) {
                    case "creators":
                        property.value.each {creator->
                            creator.creatorType == 'author'\
                                ?authors.add(creator.firstName + " " + creator.lastName)
                                :editors.add(creator.firstName + " " + creator.lastName)

                            Vertex person = g.query().has("firstName", creator.firstName).has("lastName", creator.lastName).vertices().isEmpty()\
                                ? g.addVertex(null,["entry":"person","firstName":creator.firstName, "lastName":creator.lastName])\
                                : g.query().has("firstName", creator.firstName).has("lastName", creator.lastName).vertices().iterator().next()

                            Edge edge = vertex.outE("isAuthored").inV.filter{it == person}\
                                ? vertex.outE("isAuthored").inV.filter{it == person}.back(2).next()
                                : this.g.addEdge(0, vertex, person, "isAuthored")
                        }
                        break
                    case "tags":
                        // creates new vertex for each project and connects to the publication
                        List<String> tagList = new ArrayList<String>()
                        property.value.each { ttag ->
                            if (ttag.tag.contains("vveitas:")) {
                                Vertex project = g.query().has("entry","project").has("tag", ttag.tag).vertices().isEmpty()\
                                ? g.addVertex(null,["entry":"project","tag": ttag.tag])\
                                : g.query().has("entry","project").has("tag", ttag.tag).vertices().iterator().next()

                                Edge edge = vertex.outE("isTagged").inV.filter{it == project}\
                                ? vertex.outE("isTagged").inV.filter{it == project}.back(2).next()
                                : this.g.addEdge(0, vertex, project, "isTagged")
                            }
                        }
                        break

                    default:
                        vertex.setProperty(property.key,property.value)
                        break
                }

            }

            vertex.setProperty("authorslist", authors)
			String authorstext = ""
			for (def no=0;no<authors.size();no++) {
				authorstext = authorstext + no + ": " + authors[no]
				vertex.setProperty("authorstext", authorstext)
			}
			vertex.setProperty("editorslist", editors)
			String editorstext = ""
			for (def no=0;no<editors.size();no++) {
				editorstext = editorstext + no + ": " + editors[no]
				vertex.setProperty("editorstext", editorstext)
			}
		logger.debug("Updated a publication vertex {}", publication.data.title)
		}
		
		return vertex
	}

	public void updateDocumentAnnotations(Vertex vertex) {
        try{
		
		String file = vertex.getProperty("file")
		vertex.setProperty("filestamp",Utils.calculateMD5Hex(file))
		
		TreeMap documentAnnotations = new TreeMap()
		if (file != "") {
			documentAnnotations = PDFMiner.getDocumentAnnotations(file);
		}
		
		Iterator documentAnnotationsIT = documentAnnotations.keyIterator()
		while (documentAnnotationsIT.hasNext()) {
			def page = documentAnnotationsIT.next()
			HashMap pageAnnotations = documentAnnotations.get(page)
			Set processedAnnotations = [] as Set
			Set unprocessedAnnotations = [] as Set
			pageAnnotations.keySet().each{unprocessedAnnotations.add(it)}
			
			//processing 'Highlight', 'Underline' and 'Square' annotations
			pageAnnotations.each{String key, Annotation annotation ->
				if (annotation.parameters.get("type") == "Highlight" ||
					annotation.parameters.get("type") == "Underline" ||
					annotation.parameters.get("type") == "Square") {
					def annotationVertex = g.query().has("key",key).vertices().isEmpty()\
						? g.addVertex()\
						: g.query().has("key",key).vertices().iterator().next()
					Edge edge = vertex.outE("isAnnotated").inV.filter{it == annotationVertex}\
						? vertex.outE("isAnnotated").inV.filter{it == annotationVertex}.back(2).next()
						: this.g.addEdge(0, vertex, annotationVertex, "isAnnotated") 
					edge.setProperty("creationDate", annotation.parameters.get("creationDate").getTime())
					annotation.parameters.each { name, value ->
						if (!name.toString().equals("creationDate") && value != null) {
							annotationVertex.setProperty(name, value)
						}
					}
					
					if (annotation.parameters.get("contents") != null && annotation.parameters.get("contents") != "þÿ") {
						def commentVertex = g.query().has("key",key+"_comment").vertices().isEmpty()\
						? g.addVertex()\
						: g.query().has("key",key+"_comment").vertices().iterator().next()
						
						edge = annotationVertex.outE("isCommented").inV.filter{it == commentVertex}\
						? annotationVertex.outE("isCommented").inV.filter{it == commentVertex}.back(2).next() \
						: this.g.addEdge(0, annotationVertex, commentVertex, "isCommented")

						commentVertex.setProperty("key", annotationVertex.getProperty("key") + "_comment")
						commentVertex.setProperty("type", "comment")
						commentVertex.setProperty("contents", annotation.parameters.get("contents"))
						
						

						/*
						 * this one sets content to text (extracted) and then removes the text property
						 * it does not work until highlighted text extraction is fixes...
						 */
						
					}
					
					annotationVertex.setProperty("contents", "")
						
					processedAnnotations.add(key)
					unprocessedAnnotations.remove(key)
					assert processedAnnotations.intersect(unprocessedAnnotations).isEmpty()
				}
			}
			
			// finding comments on 'Highlight' , 'Underline' and 'Square' annotations
			Set iterator = [] as Set
			processedAnnotations.each{iterator.add(it)}
			iterator.each{keyProcessed ->
				Annotation processedAnn = pageAnnotations.get(keyProcessed)
				if (processedAnn.parameters.get("rectangle") !=null) {
					PDRectangle rectangle = Utils.deserializePDRectangle(processedAnn.parameters.get('rectangle'))
					PDRectangle expanded = Utils.expandRectangleBy(rectangle, 20f)
					Set iterator2 = [] as Set
					unprocessedAnnotations.each{iterator2.add(it)}
					iterator2.each{keyUnprocessed ->
						assert keyProcessed !=keyUnprocessed
						Annotation unprocessedAnn = pageAnnotations.get(keyUnprocessed)
						if (unprocessedAnn.parameters.get("type") == "Text") {
							PDRectangle childRectangle = Utils.deserializePDRectangle(unprocessedAnn.parameters.get('rectangle'))
							if (expanded.contains(childRectangle.getLowerLeftX(), childRectangle.getLowerLeftY())) {
								def childAnnotationVertex = g.query().has("key",keyUnprocessed).vertices().isEmpty()\
								? g.addVertex()\
								: g.query().has("key",keyUnprocessed).vertices().iterator().next()
								Vertex annotationVertex = this.g.getVertices("key",keyProcessed).iterator().next()
								
								Edge childEdge = annotationVertex.outE("isCommented").inV.filter{it == childAnnotationVertex}\
								? annotationVertex.outE("isCommented").inV.filter{it == childAnnotationVertex}.back(2).next() \
								: this.g.addEdge(0, annotationVertex, childAnnotationVertex, "isCommented")
								
								childEdge.setProperty("creationDate", unprocessedAnn.parameters.get("creationDate").getTime())
								
								unprocessedAnn.parameters.each { key, value ->
									if (!key.toString().equals("creationDate") && value != null) {
										childAnnotationVertex.setProperty(key, value)
									}
								}
								childAnnotationVertex.setProperty("type", "comment")
								processedAnnotations.add(keyUnprocessed)
								unprocessedAnnotations.remove(keyUnprocessed)
								assert processedAnnotations.intersect(unprocessedAnnotations).isEmpty()
							}
						}
					}
				}
			}
			// processing the rest of "Text" annotations
			iterator = [] as Set
			unprocessedAnnotations.each{iterator.add(it)}
			iterator.each{keyUnprocessed ->
				Annotation unprocessedAnn = pageAnnotations.get(keyUnprocessed)
				if (unprocessedAnn.parameters.get("type") == "Text") {
					def annotationVertex = g.query().has("key",keyUnprocessed).vertices().isEmpty()\
					? g.addVertex()\
					: g.query().has("key",keyUnprocessed).vertices().iterator().next()
					Edge annotationEdge = vertex.outE("isAnnotated").inV.filter{it == annotationVertex}\
					? vertex.outE("isAnnotated").inV.filter{it == annotationVertex}.back(2).next()
					: this.g.addEdge(0, vertex, annotationVertex, "isAnnotated")
					/*
					Edge annotationEdge = annotationVertex.inE("isAnnotated").outV.filter{it == vertex}\
					? annotationVertex.inE("isAnnotated").outV.filter{it == vertex}.back(2).next()\
					: this.g.addEdge(0, vertex, annotationVertex, "isAnnotated")
					*/
					annotationEdge.setProperty("creationDate", unprocessedAnn.parameters.get("creationDate").getTime())
					unprocessedAnn.parameters.each { key, value ->
						if (!key.toString().equals("creationDate") && value != null) {
							annotationVertex.setProperty(key, value)
						}
					}
					processedAnnotations.add(keyUnprocessed)
					unprocessedAnnotations.remove(keyUnprocessed)
					assert processedAnnotations.intersect(unprocessedAnnotations).isEmpty()
				}
			}
		}
	}

												
	public void createNewLibraryGraph() {
		List<Entry> libraryEntries = Utils.getZoteroLibrary()
		logger.debug("Retrieved Zotero library")
		List<String> zotero_keys = Utils.getZoteroItemKeys(libraryEntries)
		logger.debug("Retrieved Zotero keys")
		Map<String,String> bib_keys = Utils.getBibtexKeys(zotero_keys)
		logger.debug("Retrieved Bibtex keys")
		Map<String,String> filenames = Utils.getFileNames(zotero_keys)
		logger.debug("Retrieved filenames")

		for (int x = 0;x<libraryEntries.size();x++) {
			this.g.commit()
			// put entry to the graph
			Entry entry = libraryEntries[x] 
			def zotero_key = Utils.getZoteroItemKey(entry)
			def bib_key = bib_keys.get(zotero_key)
			def filename = filenames.get(zotero_key)?: ""
			print "Processing: "+ zotero_key+"; "+bib_key+"; "+ filename
			
			Vertex vertex = updatePublicationVertex(entry, zotero_key, bib_key, filename)
			
			//get annotations from the pdf file
			String file = vertex.getProperty("file")
			String filestamp = vertex.getProperty("filestamp")
			if (file != "" && (filestamp == null || filestamp != Utils.calculateMD5Hex(file))) {
					updateDocumentAnnotations(vertex)
			}
			this.g.commit()
			println "..... Done." 
		}
	}


    public void updateProjectPublications(String project) {
        List libraryEntries = Utils.getZoteroLibrary(project)
        logger.debug("Retrieved Zotero library")
        List<String> zotero_keys = Utils.getZoteroItemKeys(libraryEntries)
        logger.debug("Retrieved Zotero keys")
        Map<String,String> bib_keys = Utils.getBibtexKeys(zotero_keys)
        logger.debug("Retrieved Bibtex keys")
        Map<String,String> filenames = Utils.getFileNames(zotero_keys)
        logger.debug("Retrieved filenames")

        for (int x = 0;x<libraryEntries.size();x++) {
            this.g.commit()
            // put entry to the graph
            Map entry = libraryEntries[x]
            def zotero_key = Utils.getZoteroItemKey(entry)
            def bib_key = bib_keys.get(zotero_key)
            def filename = filenames.get(zotero_key)?: ""
            print "Processing: "+ zotero_key+"; "+bib_key+"; "+ filename

            Vertex vertex = updatePublicationVertex(entry, zotero_key, bib_key, filename)

            //get annotations from the pdf file
            String file = vertex.getProperty("file")
            String filestamp = vertex.getProperty("filestamp")
            if (file != "" && (filestamp == null || filestamp != Utils.calculateMD5Hex(file))) {
                updateDocumentAnnotations(vertex)
            }
            this.g.commit()
            println "..... Done."
        }
    }


    public Map fullTextSearch(ArrayList fields, String query) {
		def resultsMap = [:]
		
		def pubList = []
		if (fields.contains("authorstext")) {
			this.g.indexQuery("search","v.authorstext:($query)").vertices().each{
				pubList.add(it.getElement().key)	
			}
		}
		if (fields.contains("editorstext")) {
			this.g.indexQuery("search","v.editorstext:($query)").vertices().each{
				pubList.add(it.getElement().key)	
			}
		}
		if (fields.contains("title")) {
			this.g.indexQuery("search","v.title:($query)").vertices().each{
				pubList.add(it.getElement().key)
			}
		}
		resultsMap.put("publications",pubList)
		
		def highList = []
		def commList = []
		if (fields.contains("contents")) { 
			this.g.indexQuery("search","v.contents:($query)").vertices().each{
				if (it.getElement().type == "Text") {
					highList.add(it.getElement().key)
				}
				if (it.getElement().type == "comment") {
					commList.add(it.getElement().key)
				}
			}
		}
		resultsMap.put("highlights", highList)
		resultsMap.put("comments", commList)
		println resultsMap
		return resultsMap
	}
}
