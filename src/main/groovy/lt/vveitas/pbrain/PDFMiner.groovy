package lt.vveitas.pbrain;

import com.thinkaurelius.titan.core.TitanGraph
import com.thinkaurelius.titan.core.TitanFactory

import com.tinkerpop.frames.FramedGraph

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle
import org.apache.pdfbox.pdmodel.graphics.color.PDGamma;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationTextMarkup;
import org.apache.pdfbox.util.PDFTextStripperByArea;

import org.apache.commons.lang3.RandomStringUtils

public class PDFMiner {
	
	static documentFileName;
		
	static File logger = new File(Parameters.parameters.logfile)

	public static TreeMap getDocumentAnnotations(String fileName) {
		String library_dir = Parameters.parameters.main_dir + Parameters.parameters.library_dir
		this.documentFileName = library_dir + fileName
		def annotations
		try {
			PDDocument document = PDDocument.load(new File(this.documentFileName));
			PDFMiner.scanAllAnnotations(document)
			annotations = PDFMiner.getAllAnnotations(document)
			document.close()
		} catch (FileNotFoundException e) {
			annotations = [:]
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			println fileName
			e.printStackTrace()
			annotations = [:]
		} finally {
			return annotations
		}
	}

	public static TreeMap getDocumentAnnotations(ArrayList annotations, String fileName) {
		String library_dir = Parameters.parameters.main_dir + Parameters.parameters.library_dir
		this.documentFileName = library_dir + fileName
		try {
			PDDocument document = PDDocument.load(new File(this.documentFileName));
			PDFMiner.scanAllAnnotations(document)
			annotations = PDFMiner.getAnnotations(annotations, document)
			document.close()
		} catch (FileNotFoundException e) {
			annotations = [:]
		} catch (Throwable e) {
			System.err.println(e.getMessage());
			println fileName
			e.printStackTrace()
			annotations = [:]
		} finally {
			return annotations
		}
	}


	/**
	 * Due to the unfortunate fact that Adobe Reader for Android does not create a key for the annotation,
	 * we have to scan the document for key == null and recreate them. After that the document has to be
	 * saved back to the pdf.
	 */

	public static boolean scanAllAnnotations(PDDocument document) throws IOException {

		TreeMap docAnns = new TreeMap();
		ArrayList allPages = (ArrayList) document.getDocumentCatalog().getAllPages();
		def modified = false
		
		for (int i = 1; i <= document.getNumberOfPages(); i++) {
			def page = allPages.get(i-1)
			
			List<PDAnnotation> la = page.getAnnotations();
			HashMap<String,Annotation> pageAnns = new HashMap<String,Annotation>();
			
			if (la.size() > 0) {
				for (int x=0; x<la.size();x++) {
					def ann = la.get(x);
					def subtype = ann.getSubtype()
					if (subtype.equals("Text")
						|| subtype.equals("Highlight") // get all annotations that interest me
						|| subtype.equals("Square")
						|| subtype.equals("Underline")
						|| subtype.equals("Line")) {
						if (ann.getAnnotationName() == null) {
							ann.setAnnotationName("pbrain-"+RandomStringUtils.randomAlphanumeric(32))
							modified = true
						}
					}
				}
			}
		}
		if (modified) {
			document.save(documentFileName)
			
		}
		document.close();
		return modified
	}
				
	
	public static HashMap<String,Annotation> getAllAnnotations(PDPage page, int pageNum) throws IOException {
		List<PDAnnotation> la = page.getAnnotations();
		HashMap<String,Annotation> pageAnns = new HashMap<String,Annotation>();
		if (la.size() > 0) {
			for (int x=0; x<la.size();x++) {
				def ann = la.get(x);
				def subtype = ann.getSubtype()
				if (subtype.equals("Text")
					|| subtype.equals("Highlight") // get all annotations that interest me
					|| subtype.equals("Square")
					|| subtype.equals("Underline")
					|| subtype.equals("Line")) {
					def key = ann.getAnnotationName()
					pageAnns.put(key,new Annotation());
					Annotation myAnn = pageAnns.get(key);
					myAnn.parameters.put("page", pageNum);
					myAnn.parameters.put("type",ann.getSubtype());
					myAnn.parameters.put("contents",ann.getContents());
					myAnn.parameters.put("key",key);
					myAnn.parameters.put("rectangle", Utils.serialize(ann.getRectangle()));
					def dict = ann.getDictionary();
					myAnn.parameters.put("creationDate", dict.getDate("M"));
					myAnn.parameters.put("author", dict.getString("T"))
					/*
					if (subtype.equals("Highlight") || subtype.equals("Underline")) {
						myAnn.parameters.put("quadPoints", ann.getQuadPoints().toList())
						myAnn.parameters.put("text", myAnn.extractHighlightedText(page))
					}
					if (subtype.equals("Square")) {
						myAnn.parameters.put("imagePath", PDFMiner.saveImageToFile(page, ann.getRectangle()))
					}
					*/
				}
			}
		}
		return pageAnns;
	}
	

	/**
	* Takes a list of annotation types as a parameter:
	* ["Text", "Highlight", "Square", "Underline", "Line"]
	*/
	public static HashMap<String,Annotation> getAnnotations(ArrayList annotations, PDPage page, int pageNum) throws IOException {
		List<PDAnnotation> la = page.getAnnotations();
		HashMap<String,Annotation> pageAnns = new HashMap<String,Annotation>();
		if (la.size() > 0) {
			for (int x=0; x<la.size();x++) {
				def ann = la.get(x);
				def subtype = ann.getSubtype()
				if (subtype in annotations) {
					def key = ann.getAnnotationName()
					pageAnns.put(key,new Annotation());
					Annotation myAnn = pageAnns.get(key);
					myAnn.parameters.put("page", pageNum);
					myAnn.parameters.put("type",ann.getSubtype());
					myAnn.parameters.put("contents",ann.getContents());
					myAnn.parameters.put("key",key);
					myAnn.parameters.put("rectangle", Utils.serialize(ann.getRectangle()));
					def dict = ann.getDictionary();
					myAnn.parameters.put("creationDate", dict.getDate("M"));
					myAnn.parameters.put("author", dict.getString("T"))
					if (subtype.equals("Highlight") || subtype.equals("Underline")){
						myAnn.parameters.put("quadPoints", ann.getQuadPoints().toList())
						myAnn.parameters.put("text", myAnn.extractHighlightedText(page))
					}
					if (subtype.equals("Square")) {
						myAnn.parameters.put("imagePath", PDFMiner.saveImageToFile(page, ann.getRectangle()))
					}
				}
			}
		}
		return pageAnns;
	}


	public static TreeMap getAllAnnotations(PDDocument document) throws IOException {
		//getting all relevant annotations from the document
		TreeMap docAnns = new TreeMap();
		ArrayList allPages = (ArrayList) document.getDocumentCatalog().getAllPages();
		for (int i = 1; i <= document.getNumberOfPages(); i++) {
			def page = allPages.get(i-1)
			docAnns.put(i,getAllAnnotations(page,i));
		}
		document.close();
		return docAnns;
	}

	public static TreeMap getAnnotations(ArrayList annotations, PDDocument document) throws IOException {
		//getting all relevant annotations from the document
		TreeMap docAnns = new TreeMap();
		ArrayList allPages = (ArrayList) document.getDocumentCatalog().getAllPages();
		for (int i = 1; i <= document.getNumberOfPages(); i++) {
			def page = allPages.get(i-1)
			docAnns.put(i,getAnnotations(annotations, page,i));
		}
		document.close();
		return docAnns;
	}

	
	public static byte[] cropRectangleToImage(PDPage page, PDRectangle rectangle) throws Throwable {
		PageCopy pageCopy =  new PageCopy(page);
		pageCopy.page.setCropBox(rectangle)
		int resolutionDotsPerInch = 300
		BufferedImage image = pageCopy.page.convertToImage(BufferedImage.TYPE_BYTE_GRAY, resolutionDotsPerInch)
		return imageToByteArray(image)
	}
	
	/*
	 * returns ablsolute path to the file
	 */
	public static String saveImageToFile(PDPage page, PDRectangle rectangle) throws Throwable {
		PageCopy pageCopy =  new PageCopy(page);
		pageCopy.page.setCropBox(rectangle)
		int resolutionDotsPerInch = 300
		BufferedImage image = pageCopy.page.convertToImage(BufferedImage.TYPE_BYTE_GRAY, resolutionDotsPerInch)
		File imageDir = new File(Parameters.parameters.main_dir + Parameters.parameters.image_dir)
		Utils.createDirIfNeeded(imageDir)
		File file = new File(imageDir, RandomStringUtils.randomAlphanumeric(16) + ".png");
		ImageIO.write(image, "png", file);
		return file.getAbsolutePath()

	}
	
	/*
	 * http://stackoverflow.com/questions/7895545/how-do-i-get-something-like-an-anonymous-inner-class-in-groovy
	 */
	public static byte[] imageToByteArray(BufferedImage image) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);
		return baos.toByteArray();
	}
	
	
	
}