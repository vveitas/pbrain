package lt.vveitas.pbrain;

import org.graphstream.stream.thread.ThreadProxyPipe
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.java.GremlinPipeline

import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.util.ElementHelper
import groovy.swing.SwingBuilder
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

public class GraphStreamSource extends ThreadProxyPipe {
	Network network
	
	public GraphStreamSource() {
		this.network = Globals.network
	}
	
	// the placeholder of unique ids of elements for which events are already generated
	private List generatedEvents = []
	// lists of properties to be included in generated events
	private List<String> edgeProperties
	private List<String> vertexProperties
	
	public registerEdgeProperties(List<String> edgeProperties) {
		this.edgeProperties = edgeProperties
	}
	
	public registerVertexProperties(List<String> vertexProperties) {
		this.vertexProperties = vertexProperties
	}
	
	public createViewFromVertex(Vertex vertex) {
		generateEventsFrom(vertex)
		GremlinPipeline pipe = new GremlinPipeline();
		pipe.start(vertex).bothE()
		pipe.each{
			generateEventsFrom(it)
		}
	}
	
	public generateEventsFrom(Edge edge) {
		def edgeid = edge.getId().toString()
		if (!generatedEvents.contains(edgeid)) {
			generateEventsFrom(edge.getVertex(Direction.IN))
			generateEventsFrom(edge.getVertex(Direction.OUT))
			
			sendEdgeAdded("network", 
				edgeid,
				edge.getVertex(Direction.OUT).getId().toString(),
				edge.getVertex(Direction.IN).getId().toString(),
				true)

			properties.each {propertykey, value ->
				if(edgeProperties.contains(propertykey)) {
					sendEdgeAttributeAdded("network", edgeid,
						propertykey, value)
				}
			}

			generatedEvents.add(edge.getId().toString())
		}
	}
	
	public generateEventsFrom(Vertex vertex) {
		def vertexkey = vertex.getId().toString()
		if (!generatedEvents.contains(vertexkey)) {
			sendNodeAdded("network",vertex.getId().toString())
			Map properties = ElementHelper.getProperties(vertex)
			if (vertex.getProperty('entry')!=null && vertex.getProperty('entry') == "publication") {
				sendNodeAttributeAdded("network", vertex.getId().toString(),"ui.class",vertex.getProperty('entry'))
				String title =  vertex.getProperty('title')
				if (title.length() > 45) {
					title = title.substring(0,45).concat("...")
				}
				sendNodeAttributeAdded("network", vertex.getId().toString(),"ui.label",title)
			} else {
				sendNodeAttributeAdded("network", vertex.getId().toString(),"ui.class",vertex.getProperty('type'))
				if (vertex.getProperty('type') != 'Square') {
					String contents =  vertex.getProperty('contents')
					if (contents.length() > 45) {
						contents = contents.substring(0,45).concat("...")
					}
				sendNodeAttributeAdded("network", vertex.getId().toString(),"ui.label",contents)
				}
			}
			
			properties.each {propertykey, value ->
				if(vertexProperties.contains(propertykey)) {
					sendNodeAttributeAdded("network", vertexkey,
						propertykey, value)
				}
			}
		generatedEvents.add(vertex.getId().toString())
		}
	}	
	
	public expandFromVertex(String id) {
		def vertex = network.g.getVertex(id)
		GremlinPipeline pipe = new GremlinPipeline();
		pipe.start(vertex).bothE()
		pipe.each{
			generateEventsFrom(it)
		}
	}
	
	public openDocument(String id) {
		def vertex = network.g.getVertex(id)
		def page = vertex.page?: 1
		def key = vertex.key
		while (vertex.getProperty("file") == null) {
			vertex = vertex.in.next()
		}
		String fileName = vertex.getProperty("file")
		String main_dir = Parameters.parameters.main_dir
		String library_dir = main_dir + Parameters.parameters.library_dir
		String command = 'evince '	+ '--page-label=' + page + ' ' + library_dir + fileName
		command.execute()
	}
	
	public Object getAnnotationContents(String id) {
		def contents
		def vertex = network.g.getVertex(id)
		def type = vertex.type
		if (type == "Text" || type == "Highlight" || type == "Underline") {
			contents = vertex.contents
		}
		if (type == "Square") {
			try {
				contents = ImageIO.read(new File(vertex.getProperty("imagePath")));
			} catch (IOException e) {
			}
		}
		return contents
	}

	public String getVertexProperties(String id) {
		def vertex = network.g.getVertex(id)
		def properties = "<html>"
		properties = properties + "<p>Vertex id: " + vertex.getId() + "</p>"
		properties = properties + "</html>"
		return properties
	}

	
}
