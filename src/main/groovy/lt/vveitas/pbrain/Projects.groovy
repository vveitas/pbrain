package lt.vveitas.pbrain

import groovy.swing.SwingBuilder

import javax.swing.*

public class Projects {
	Network network = Globals.network
	SwingBuilder swing

	public Projects(Map filterParameters) {
        this.swing = new SwingBuilder()
        List<String> tags = Utils.getAllTags()

        def projects

        def updateProject = {

            def project
            projects.components.each{ radio ->
                if(radio.selected ==true) {project =  radio.name}
            }

            network.updateProjectPublications(project)

        }

        def selectedRadioButton = ""

        def panel = swing.frame(title: 'Projects in Zotero Database',
                defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE,
                size: [400, 250],
                show: true,
                location: filterParameters.location) {

            panel() {
                boxLayout(axis: BoxLayout.Y_AXIS)

                projects = panel(border: BorderFactory.createTitledBorder("Projects")) {
                    boxLayout(axis: BoxLayout.Y_AXIS)
                    tags.each {
                        swing.radioButton(text: it, name: it)
                    }
                }

                def button = panel() {
                    swing.button(text: "update selectedProjects", actionPerformed: updateProject, alignmentX: 0.5f)
                }
            }
        }
    }
}
