package lt.vveitas.pbrain

import groovy.swing.SwingBuilder

import javax.swing.JFrame
import javax.swing.BoxLayout

class Dashboard {
	Network network = Globals.network
	SwingBuilder swing
	
	public Dashboard() {
		
		def updateDatabase = {
			network.createNewLibraryGraph(null)
		}
		
		def infoPanelWithAllPublications = {
			Globals.openFrames.add(new DataPanel([location:[500,200]]))
		}

        def zoteroDatabaseStats = {
            Globals.openFrames.add(new Projects([location:[500,200]]))
        }
		
		def updateDatabaseButton = { swing.button(text: "import all publications in Zotero", actionPerformed: updateDatabase, alignmentX: 0.5f) }
		def allPublicationsButton = { swing.button(text: "display imported publications ", actionPerformed: infoPanelWithAllPublications, alignmentX: 0.5f) }
        def zoteroDatabaseStatsButton = { swing.button(text: "select Projects to import", actionPerformed: zoteroDatabaseStats, alignmentX: 0.5f) }

		this.swing = new SwingBuilder()
		
		def dashboard = swing.frame(title: 'externalBrain dashboard',
									defaultCloseOperation: JFrame.EXIT_ON_CLOSE,
									size: [400,250],
									show: true)	{
            panel() {
                boxLayout(axis:BoxLayout.Y_AXIS)

                swing.label("*** Local Graph Database ***",alignmentX: 0.5f)
                swing.label(" ") // inserting a vertical space..
                updateDatabaseButton()
                swing.label(" ") // inserting a vertical space..
                panel(name: "Graph Data") {
                    tableLayout{
                        tr{
                            td{swing.label("Total publications: ")}
                            td{swing.label(network.g.V('entry','publication').count().toString())}
                        }
                        tr{
                            td{swing.label("Anonotated publications: ")}
                            td{swing.label(network.g.V('entry','publication').filter{it.outE('isAnnotated').count()!=0}.count().toString())}
                        }
                        tr{
                            td{swing.label("Number of annotations: ")}
                            td{swing.label(network.g.V('entry','publication').outE('isAnnotated').count().toString())}
                        }

                    }
                }

                swing.label("*** Zotero Database ***",alignmentX: 0.5f)
                swing.label(" ") // inserting a vertical space..
                panel() {
                    boxLayout(axis:BoxLayout.Y_AXIS)
                    allPublicationsButton()
                    zoteroDatabaseStatsButton()
                }
                swing.label(" ") // inserting a vertical space..

            }
        }
	}
}

