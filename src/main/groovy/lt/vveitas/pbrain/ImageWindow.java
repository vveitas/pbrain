package lt.vveitas.pbrain;

import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JFrame;
import java.awt.image.BufferedImage;

public class ImageWindow {
	
	JFrame frame = new JFrame("Visual evaluation of the distribution");
	public boolean buttonPressed = false;
	
	public ImageWindow(BufferedImage bufferedImage) throws Exception {
		Panel imagePanel = new AnnotationImage(bufferedImage);
		frame.getContentPane();
		//the layout makes the panels stand side by side from left to right

		frame.setLayout(new BorderLayout());
		//adding panels
		frame.add(imagePanel, BorderLayout.CENTER);
		//setBounds combines setLocation and setSize
		frame.setBounds(100, 100, imagePanel.getWidth(), 100+imagePanel.getHeight());
		frame.setResizable(true);
		frame.setVisible(true);
	}
	
	private ComponentListener closeWindow = new ComponentListener() {
		public void componentHidden(ComponentEvent arg0) {
	    	   frame.setVisible(false);
	    	   frame.dispose();
	    	   buttonPressed = true;
		}

		public void componentMoved(ComponentEvent e) {
			// NOT IMPLEMENTED
			
		}

		public void componentResized(ComponentEvent e) {
			// NOT IMPLEMENTED
			
		}

		public void componentShown(ComponentEvent e) {
			// NOT IMPLEMENTED
		}
	};
}
