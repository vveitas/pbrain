package lt.vveitas.pbrain

import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.pipes.util.structures.Table
import com.tinkerpop.gremlin.Tokens.T

import groovy.swing.SwingBuilder
import groovy.beans.Bindable

import java.awt.BorderLayout
import java.awt.GridLayout
import javax.swing.BoxLayout
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JTabbedPane
import javax.swing.JTable
import javax.swing.JTextArea
import javax.swing.JToggleButton
import javax.swing.SwingUtilities
import javax.swing.text.View

import java.awt.Dimension
import java.awt.GridLayout
import java.awt.Color
import java.awt.Point
import java.awt.MouseInfo
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.beans.PropertyChangeListener
import java.nio.channels.NetworkChannel;

public class DataPanel {
	Network network = Globals.network
	GraphStreamSource eventGenerator
	SwingBuilder swing
	
	// adapted from http://www.ibm.com/developerworks/java/library/j-groovy09299/
	
	public DataPanel(Map filterParameters) {
		eventGenerator = new GraphStreamSource()
		def searchText
		def searchFields
		def checkBoxList
		Browser browser1
		Browser browser2
		
		
		def publications = []
		def highlights = []
		def comments = []
        def projects = []
        def persons = []

		if (filterParameters.publications != null) {
			if (filterParameters.publications == []) { filterParameters.publications = ["giberish"] }
			network.g.V('entry','publication')
				.has('key',T.in,filterParameters.publications).each{pub->

				publications.add([
					id:pub.id,
					title:pub.title.toString(),
					authors:pub.authorslist.toString(),
					editors:pub.editorslist.toString(),
					type:pub.type.toString(),
					numberOfAnnotations: pub.outE('isAnnotated').count()+pub.outE.inV.outE('isCommented').count()
                    ])

                // also project tags
                // the following two will not display project tags and authors/editors if specific publication is not selected
                // therefore for following all relations better use visual browser...

                pub.outE('isTagged').inV.each {
                    if (!(it.id in projects.id)) {
                        projects.add([
                                id                  : it.id,
                                tag                 : it.tag,
                                numberOfPublications: it.inE('isTagged').count()
                        ])
                    }
                }

                // and maybe authors / editors

                pub.outE('isAuthored').inV.each {
                    if (!(it.id in persons.id)) {
                        persons.add([
                                id       : it.id,
                                firstName: it.firstName,
                                lastName : it.lastName,
                                numberOfPublications: it.inE('isAuthored').count()
                        ])
                    }
                }
            }
				
			if (filterParameters.highlights == null) {
				network.g.V('entry','publication')
					.has('key',T.in,filterParameters.publications).each{pub ->
						def pubTitle = pub.title.toString()
						pub.outE('isAnnotated').inV.hasNot('contents',"").order{it.a.page <=> it.b.page}.each{ann ->
							highlights.add([
							highlightId:ann.id,
							publication:pubTitle,
							contents:ann.contents.toString(),
							comments:ann.bothE('isCommented').count(),
							relations:ann.bothE('isRelated').count(),
							page:ann.page
						])
					}
				}
			} else {
				network.g.V('entry','publication')
				 .outE('isAnnotated').inV.has('key',T.in,filterParameters.highlights).each{ highlight ->
					highlights.add([
						highlightId:highlight.id,
						publication:highlight.inE('isAnnotated').outV.has('entry','publication').next().title.toString(),
						contents:highlight.contents.toString(),
						comments:highlight.bothE('isCommented').count(),
						relations:highlight.bothE('isRelated').count(),
						page:highlight.page
					])
				}
			}

			if (filterParameters.comments == null) {
				network.g.V('entry','publication')
					.has('key',T.in,filterParameters.publications).each{pub ->
						def pubTitle = pub.title.toString()
						pub.outE('isAnnotated').inV.order{it.a.page <=> it.b.page}.each{ann ->
							ann.outE('isCommented').inV.each{comment ->
								comments.add([
									commentId:comment.id,
									publication:pubTitle,
									contents:comment.contents.toString(),
									relations:comment.bothE('isRelated').count(),
									page:comment.inE('isCommented').outV.page.next()
									])
							}
						}
					}
			} else {
				network.g.V('entry','publication')
					.outE('isAnnotated').inV.outE('isCommented').inV.has('key',T.in,filterParameters.comments).each{ comment ->
						comments.add([
							commentId:comment.id,
							publication:comment.inE('isCommented').outV.inE('isAnnotated').outV.has('entry','publication').next().title.toString(),
							contents:comment.contents.toString(),
							relations:comment.bothE('isRelated').count(),
							page:comment.inE('isCommented').outV.page.next()
						])
					}
				}

		} else {
		
			// if filterParemeters map is empty we display all publications but no highlights or annotations,
			// because it will be simply to many of them an may slow down the whole thing
			// highlights and annotations should be opened from infoPanel by selecting a publication
		
			network.g.V('entry','publication').each{
				publications.add([
					id:it.id,
					key:it.key,
					title:it.title.toString(),
					authors:it.authorslist.toString(),
					editors:it.editorslist.toString(),
					type:it.type.toString(),
					numberOfAnnotations: it.outE('isAnnotated').count()+it.outE.inV.outE('isCommented').count()
					])
			}

            // also all project tags

            network.g.V('entry','project').each{
                projects.add([
                        id:it.id,
                        tag:it.tag,
                        numberOfPublications: it.inE('isTagged').count()
                ])
            }

            // and maybe authors / editors

            network.g.V('entry','person').each{
                persons.add([
                        id:it.id,
                        firstName:it.firstName,
                        lastName:it.lastName,
                        numberOfPublications: it.inE('isAuthored').count()
                ])
            }


        }

		def highlightsTable = {
			swing.table(id:'highlightsModel'){
			   tableModel(list: highlights){
				   propertyColumn(header: "Highlights" ,propertyName: 'contents', preferredWidth: 600)
				   propertyColumn(header: "Relations" ,propertyName: 'relations', preferredWidth: 50)
				   propertyColumn(header: "Comments" ,propertyName: 'comments', preferredWidth: 50)
				   propertyColumn(header: "Publication" ,propertyName: 'publication', preferredWidth: 100)
				   propertyColumn(header: "Page" ,propertyName: 'page', preferredWidth: 50)
				   
				}
			}
		}

		def commentsTable = {
			swing.table(id:'commentsModel'){
			   tableModel(list: comments){
				   propertyColumn(header: "Comment" ,propertyName: 'contents', preferredWidth: 600)
				   propertyColumn(header: "Relations" ,propertyName: 'relations', preferredWidth: 50)
				   propertyColumn(header: "Publication" ,propertyName: 'publication', preferredWidth: 100)
				   propertyColumn(header: "Page" ,propertyName: 'page', preferredWidth: 50)
				   
				}
			}
		}

		def publicationsTable = {
			swing.table(id:'publicationsModel'){
				def model = tableModel(list: publications){
				   propertyColumn(header: "Type",propertyName: 'type', preferredWidth: 30)
				   propertyColumn(header: "Title" ,propertyName: 'title', preferredWidth: 400)
				   propertyColumn(header: "Authors" ,propertyName: 'authors', preferredWidth: 75)
				   propertyColumn(header: "Editors", propertyName: 'editors', preferredWidth: 75)
				   propertyColumn(header: "Annotations" ,propertyName: 'numberOfAnnotations', preferredWidth: 50)
				}
			}
		}

        def projectsTable = {
            swing.table(id:'projectsModel'){
                def model = tableModel(list: projects){
                    propertyColumn(header: "Project",propertyName: 'tag', preferredWidth: 200)
                    propertyColumn(header: "Publications",propertyName: 'numberOfPublications', preferredWidth: 50)
                }
            }
        }

        def personsTable = {
            swing.table(id:'personsModel'){
                def model = tableModel(list: persons){
                    propertyColumn(header: "Last Name",propertyName: 'lastName', preferredWidth: 100)
                    propertyColumn(header: "First Name",propertyName: 'firstName', preferredWidth: 100)
                    propertyColumn(header: "Publications",propertyName: 'numberOfPublications', preferredWidth: 50)
                }
            }
        }


        this.swing = new SwingBuilder()
		
		def displayText = { String text ->
			JFrame frame = swing.frame(title: 'Annotation',
				defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE,
				undecorated: true)
			
			Point mouseLocation = MouseInfo.getPointerInfo().getLocation()
			frame.setLocation(mouseLocation)
			
			def colNo = 40
			def rowNo = (int) Math.ceil(text.size() / colNo)
			
			def textArea = new JTextArea()
			textArea.setText(text)
			textArea.setLineWrap(true)
			textArea.setWrapStyleWord(true)
			textArea.setBackground(Color.WHITE)
			textArea.setColumns(colNo)
			textArea.setRows(rowNo)
				
			frame.getContentPane().add(textArea)
			frame.pack()
			
			frame.setAlwaysOnTop(true)
			frame.setVisible(true)

		}
		
		def filter = {
			searchFields = checkBoxList.findAll{it.selected}.collect{it.text}
			def resultsMap = network.fullTextSearch(searchFields,searchText.text)
			resultsMap.put("location",[200,200]) 
			new DataPanel(resultsMap)
		}
		
		def filterPanel = {
			swing.panel(name:'Filter', constraints: BorderLayout.NORTH) {
				searchText = textField(columns:35, actionPerformed: filter)
				checkBoxList = [checkBox(id: "contents", text: "contents", selected: true),
								checkBox(id: "authorstext", text: "authorstext", selected: true),
								checkBox(id: "editorstext", text: "editorstext", selected: true),
								checkBox(id: "title", text: "title", selected: true),
								]
				button(text:"Filter", actionPerformed: filter)
			}
		}
		
		def publicationsPanel = {
			swing.scrollPane(title:'Publications'){
				
				def table = publicationsTable()
				
				MouseListener mouseListenerPublications = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						int row = table.rowAtPoint(e.getPoint());
						int col = table.columnAtPoint(e.getPoint());
						table.setRowSelectionInterval(row,row)
						
						if (SwingUtilities.isRightMouseButton(e)) {
							swing.popupMenu(location: [e.getX(), e.getY()]) {
								menuItem{
									action(name:"Open publication", closure: {
										def vertex = network.g.getVertex(publications[row].id)
										String fileName = vertex.getProperty("file")
										Utils.openDocumentOkular(fileName)
									})
								}
								menuItem{
									action(name:"Publication's annotations", closure:{
										new DataPanel([publications:[publications[row].key],
											location:[table.getLocationOnScreen().getX().intValue() +10,table.getLocationOnScreen().getY().intValue()+10]])})
								}
								menuItem{
									action(name:"Delete from database", closure:{
										def vertex = network.g.getVertex(publications[row].id)
										vertex.outE('isAnnotated').inV().outE('isCommented').inV().each{network.g.removeVertex(it)}
										vertex.outE('isAnnotated').inV().each{network.g.removeVertex(it)}
										network.g.removeVertex(vertex)
										network.g.commit()
										})
								}
								menuItem{
									action(name:"Update in the database", closure:{
										def vertex = network.g.getVertex(publications[row].id)
										network.updateDocumentAnnotations(vertex)
										network.g.commit()
										})
								}
								menuItem{
									action(name:"Latex reference", closure:{
										def annotation = publications[row]
										def vertex = network.g.getVertex(annotation.id)
										String bib_key = vertex.getProperty("bib_key")
										String latex_ref = "\\cite{"+bib_key+"}"
										Toolkit toolkit = Toolkit.getDefaultToolkit()
										Clipboard clipboard = toolkit.getSystemClipboard()
										StringSelection strSel = new StringSelection(latex_ref)
										clipboard.setContents(strSel, null)

									})
								}
							}.show(e.getComponent(), e.getX(), e.getY())
						}
					}
				};
		
				table.addMouseListener(mouseListenerPublications)
			}
		}
		
		def highlightsPanel = {
			swing.scrollPane(title:'Comments on pages'){
				def table = highlightsTable()

				
				MouseListener mouseListenerHighlights = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						int row = table.rowAtPoint(e.getPoint());
						int col = table.columnAtPoint(e.getPoint());
						table.setRowSelectionInterval(row,row)
						
						if(SwingUtilities.isLeftMouseButton(e)){
							displayText(highlights[row].contents)
						}
						
						if (SwingUtilities.isRightMouseButton(e)) {
							swing.popupMenu(location: [e.getX(), e.getY()]) {
								menuItem{
									action(name:"Command to open", closure:{
										def annotation = highlights[row]
										def vertex = network.g.getVertex(annotation.highlightId)
										def page = annotation.page
										String fileName = vertex.inE('isAnnotated').outV.has('entry','publication').next().getProperty("file")
										String path = Parameters.parameters.main_dir + Parameters.parameters.library_dir + fileName
										String command = "acroread /a 'page=$page' '$path'"
										Toolkit toolkit = Toolkit.getDefaultToolkit()
										Clipboard clipboard = toolkit.getSystemClipboard()
										StringSelection strSel = new StringSelection(command)
										clipboard.setContents(strSel, null)
									})
								}
								menuItem{
									action(name:"Latex reference", closure:{
										def annotation = highlights[row]
										def vertex = network.g.getVertex(annotation.highlightId)
										def page = annotation.page
										String bibtex_key = vertex.inE('isAnnotated').outV.has('entry','publication').next().getProperty("bib_key")
										String latex_ref = "\\cite[p. "+page+"]{"+bibtex_key+"}"
										Toolkit toolkit = Toolkit.getDefaultToolkit()
										Clipboard clipboard = toolkit.getSystemClipboard()
										StringSelection strSel = new StringSelection(latex_ref)
										clipboard.setContents(strSel, null)

									})
								}
							}.show(e.getComponent(), e.getX(), e.getY())
						}
					}
				};
		
				table.addMouseListener(mouseListenerHighlights)
			}
		}

		def commentsPanel = {
			swing.scrollPane(title:'Comments on highlights'){
				def table = commentsTable()

				
				MouseListener mouseListenerComments = new MouseAdapter() {
					public void mouseClicked(MouseEvent e) {
						int row = table.rowAtPoint(e.getPoint());
						int col = table.columnAtPoint(e.getPoint());
						table.setRowSelectionInterval(row,row)
						
						if(SwingUtilities.isLeftMouseButton(e)){
							displayText(comments[row].contents)
						}
						
						if (SwingUtilities.isRightMouseButton(e)) {
							swing.popupMenu(location: [e.getX(), e.getY()]) {
								menuItem{
									action(name:"Command to open", closure:{
										def comment = comments[row]
										def vertex = network.g.getVertex(comment.commentId)
										def page = comment.page
										String fileName = vertex.inE('isCommented').outV
											.inE('isAnnotated').outV.has('entry','publication').next().getProperty("file")
										String path = Parameters.parameters.main_dir + Parameters.parameters.library_dir + fileName
										String command = "acroread /a 'page=$page' '$path'"
										Toolkit toolkit = Toolkit.getDefaultToolkit()
										Clipboard clipboard = toolkit.getSystemClipboard()
										StringSelection strSel = new StringSelection(command)
										clipboard.setContents(strSel, null)
									})
								}
								menuItem{
									action(name:"Latex reference", closure:{
										def annotation = comments[row]
										def vertex = network.g.getVertex(annotation.commentId)
										def page = annotation.page
										String bibtex_key = vertex.inE('isCommented').outV
											.inE('isAnnotated').outV.has('entry','publication').next().getProperty("bib_key")
										String latex_ref = "\\cite[p. "+page+"]{"+bibtex_key+"}"
										Toolkit toolkit = Toolkit.getDefaultToolkit()
										Clipboard clipboard = toolkit.getSystemClipboard()
										StringSelection strSel = new StringSelection(latex_ref)
										clipboard.setContents(strSel, null)

									})
								}
							}.show(e.getComponent(), e.getX(), e.getY())
						}
					}
				};

			table.addMouseListener(mouseListenerComments)
			}
		}

        def projectsPanel = {
            swing.scrollPane(title:'Projects'){
                def table = projectsTable()

                MouseListener mouseListenerHighlights = new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        int row = table.rowAtPoint(e.getPoint());
                        int col = table.columnAtPoint(e.getPoint());
                        table.setRowSelectionInterval(row,row)

                        if (SwingUtilities.isRightMouseButton(e)) {
                            swing.popupMenu(location: [e.getX(), e.getY()]) {
                                menuItem{
                                    action(name:"Publications of the project", closure:{
                                        ArrayList projectPublications = network.g.v(projects[row].id).inE('isTagged').outV.key.toList()
                                        new DataPanel([publications:projectPublications,
                                                       location:[table.getLocationOnScreen().getX().intValue() +10,table.getLocationOnScreen().getY().intValue()+10]])})
                                }
                            }.show(e.getComponent(), e.getX(), e.getY())
                        }
                    }
                };

                table.addMouseListener(mouseListenerHighlights)
            }
        }

        def personsPanel = {
            swing.scrollPane(title:'Authors / Editors'){
                def table = personsTable()

                MouseListener mouseListenerHighlights = new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        int row = table.rowAtPoint(e.getPoint());
                        int col = table.columnAtPoint(e.getPoint());
                        table.setRowSelectionInterval(row,row)

                        if (SwingUtilities.isRightMouseButton(e)) {
                            swing.popupMenu(location: [e.getX(), e.getY()]) {
                                menuItem{
                                    action(name:"Publications created", closure:{
                                        ArrayList createdPublications = network.g.v(persons[row].id).inE('isAuthored').outV.key.toList()
                                        new DataPanel([publications:createdPublications,
                                                       location:[table.getLocationOnScreen().getX().intValue() +10,table.getLocationOnScreen().getY().intValue()+10]])})
                                }
                            }.show(e.getComponent(), e.getX(), e.getY())
                        }
                    }
                };

                table.addMouseListener(mouseListenerHighlights)
            }
        }



        def tabbsPanel = {
			swing.tabbedPane(tabPlacement: JTabbedPane.TOP) {
                projectsPanel()
				publicationsPanel()
                personsPanel()
				highlightsPanel()
				commentsPanel()
			}
		}
		
		def infoPanel = swing.frame(title: 'externalBrain infopanel', 
										defaultCloseOperation: JFrame.DISPOSE_ON_CLOSE,
										size:[1000,500],
										show: true,
										location: filterParameters.location) {
				filterPanel()
				tabbsPanel()
		}
	}
	
}
